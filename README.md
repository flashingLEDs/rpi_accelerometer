# rpi_accelerometer

Interfacing an rpi3 with the 3-axis MMA8451 accelerometer. Using a cheap breakout board from ebay

The i2c interface on the rpi3 means data can only be streamed from one axis at 400Hz. Lower speeds are required to measure multiple axes at once.

## Hardware configuration: (rpi3)--->MMA8451 breakout board

p02 (+5V) --> VCC_IN

p39 (gnd) --> GND, SAO

p03 (SDA) --> SDA

p05 (SCL) --> SCL

## Examples of script use:

```
python3 saveStream.py x 400 testOutput.txt 1
```
streams 400 samples of channel x to the file testOutput.dat, overwriting if this file already exists.

```
python3 saveSeveralStreams.py x 400 testOutput 1 50
```
streams 400 samples of channel x, 50 times in a row. The resulting 50 files are named testOutput0.txt,testOutput1.txt ...

## remote (wifi) connection to the raspberry pi (from Ubuntu 17.04):
```
nmap -sn 192.168.1.0/24 
```
will map all devices on the network. You have to guess about which IP is the pi has since it doesnt really identify itself.

Once you think you know: (e.g. it's 7)
```
ssh 192.168.1.7 -l pi
```
default password: raspberry

To copy a file off the pi (from a remote PC):
```
scp pi@192.168.1.7:/home/pi/Desktop/rpi_accelerometer/test.txt .
```
To copy a whole directory off the pi (from a remote PC):
```
scp -r pi@192.168.1.7:/home/pi/Desktop/rpi_acelerometer .
```

## remote (wifi) connection to the raspberry pi (from win7):
Install a win7 version of nmap: https://nmap.org/download.html.

Run the Zenmap gui as administrator, and execute:
```
nmap -sn 192.168.1.0/24 
```
(Note that the MAC summary comes AFTER the IP)

Use putty to ssh into it

In the putty directory you'll also find pscp. Run through cmd (as administrator) with the same command as for ubuntu above.

## Data analysis/presentation:
Use the included jupyter notebook

