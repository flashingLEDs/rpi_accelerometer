import smbus
import time
import sys
import os
import datetime

bus=smbus.SMBus(1)

#I2C address for MMA4671
address=0x1c

def readData(axis):

	if axis=='z':
		MSB = (bus.read_byte_data(address,0x05)) 
		LSB = (bus.read_byte_data(address,0x06))
	elif axis=='y':
		MSB = (bus.read_byte_data(address,0x03)) 
		LSB = (bus.read_byte_data(address,0x04))
	else:
		MSB = (bus.read_byte_data(address,0x01)) 
		LSB = (bus.read_byte_data(address,0x02))

	isNegative=0
	if MSB & 0b10000000:
		isNegative=1
	
	result = (MSB<<6) + (LSB>>2)

	if isNegative:		
		result=(result^0b0011111111111111) +1
		result=result*-1		

	return result
	
if __name__ == '__main__':

	axisToMeasure=sys.argv[1]
	numSamples=int(sys.argv[2])
	saveFileName=sys.argv[3]
	overwriteFlag=int(sys.argv[4])
	
	if axisToMeasure not in ('x','X','y','Y','z','Z'):
		
		print("Invalid axis specified (expect one of [x,X,y,Y,z,Z]), received",axisToMeasure,". Default to x axis")
		axisToMeasure='x'

	if not os.path.isfile(saveFileName) or overwriteFlag:
		saveFile=open(saveFileName,'w')
		saveFile.write("timestamp\tx\ty\tz\n")
		saveFile.close()
	else:
		print("Specified save file already exists, will append to it")

	print("\n>>>>>>>> MMA8451 Initialize <<<<<<<<")
	print("Testing ID register...")
	result = bus.read_byte_data(address,0x0D)
	if result==0x1A:
		print("\t Pass")
	else:
		print("\t Fail")


	print("Resetting...")
	bus.write_byte_data(address,0x2B,0b01000000) 
	time.sleep(0.2)
	while(bus.read_byte_data(address,0x2B) & 0x40):
		pass
	print("\t Reset")
	

	print("Setting 2G range (250uG/count) ...")
	bus.write_byte_data(address,0x0E,0b00000000)

	print("Setting high resolution mode..")
	bus.write_byte_data(address,0x2B,0b00000010)

	print("Setting active, low noise mode, 400Hz data rate....")
	bus.write_byte_data(address,0x2A,0b00001101)
	
	print("\n>>>>>>>> MMA8451 stream to file <<<<<<<<")
	print("Measuring",axisToMeasure,"axis only, 400Hz sampling rate")
	print(datetime.datetime.now())	    
	print("Will write",numSamples,"samples to file",saveFileName," ...")
	saveFile=open(saveFileName,'a')
	startTime=datetime.datetime.now()
	pollDelta=datetime.timedelta(microseconds=2480)
	for ii in range(numSamples):
		pollTimer=datetime.datetime.now()
		resultX=0
		resultY=0
		resultZ=0
		if axisToMeasure==('x' or 'X'):
			resultX=readData('x')   
		elif axisToMeasure==('y' or 'Y'):
			resultY=readData('y')
		elif axisToMeasure==('z' or 'Z'):
			resultZ=readData('z')

		saveFile.write("{0}\t{1}\t{2}\t{3}\n".format(datetime.datetime.now(),resultX,resultY,resultZ))
	    	
		while(True):
			elapsedTime=datetime.datetime.now()-pollTimer
			if elapsedTime>=pollDelta:
				break
	saveFile.close()
	print("\n>>>>>>>> Finished <<<<<<<<")				
	endTime=datetime.datetime.now()
	
	print("Time taken:",endTime-startTime)
	elapsedTime=endTime-startTime
	print("This corresponds to an actual sample rate of",numSamples/elapsedTime.total_seconds(),"Hz")
	print("\t",100*(numSamples/elapsedTime.total_seconds())/400,"% of target rate")
			



